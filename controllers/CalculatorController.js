const Network = require('../models/network');

exports.home = (req, res) => {
    res.render('calculator');
};

exports.auto = (req, res) => {
    sess = req.session;
    res.render('auto');
};

exports.manual = (req, res) => {
    sess = req.session;
    var networksInfo = [];
    if (!sess.networkInfo)
    {
        sess.networkInfo = networksInfo;
    }
    res.render('manual', {session: sess});
};

exports.destroySession = (req, res, next) => {
    req.session.destroy((err) => {
        if(err) {
            return console.log(err);
        }
    });

    next();
};

exports.result = (req, res) => {
    sess = req.session;
    sess.networkInfo = req.networkInfo;
    sess.type = req.query.type;
    sess.minimum = req.query.calcWay;
    sess.amount = req.query.amount;
    res.render('result', {session: sess});
};

exports.calculate = (req, res, next) => {
    var type = req.query.type;
    var array = [];
    var calcNetworksInfo = [];

    if (type === "auto"){
        var amount = parseInt(req.query.amount);
        var calcWay = req.query.calcWay;
        
        if (calcWay === "minIP"){
            getSmaller(array, amount);
            getNetworkInfo(req, calcNetworksInfo, array);
        }
        else if (calcWay === "minNet"){
            getBigger(array, amount);
            getNetworkInfo(req, calcNetworksInfo, array);
        }
    }
    else if (type === "manual"){
        var networks = req.query.net;
        if  (typeof networks == "string"){
            var amount = parseInt(networks);
            getBigger(array, amount);          
        }
        else {
            networks.forEach(function(element, index) {
                if (element === "") {
                    networks[index] = 253;
                }
                else {
                    networks[index] = parseInt(element);
                }
            })

            networks.sort(function(a, b){return b-a}).forEach(function(element) {
                var amount = parseInt(element);
                getBigger(array, amount);          
            })
        }

        getNetworkInfo(req, calcNetworksInfo, array);
    }

    next();
};

function getSmaller(array, amount){
    var tempAmount = amount + 1;

    var found = Network.getSmallerNetwork(tempAmount);
    array.push(found); 
    tempAmount = tempAmount - found.amount; //amount - available hosts + router      

    if(tempAmount > 0){
        getSmaller(array, tempAmount);
    }
}

function getBigger(array, amount){
    var tempAmount = amount + 1;

    var found = Network.getBiggerNetwork(tempAmount);
    if (found != null){
        array.push(found); 
    }
    else {
        var found = Network.getSmallerNetwork(tempAmount);
        array.push(found); 
        tempAmount = tempAmount - found.amount; //amount - available hosts + router 

        if(tempAmount > 0){
            getBigger(array, tempAmount);
        }
    }
}

function getNetworkInfo(req, calcNetworksInfo, array){
    var add3rd = 0;
    var add4th = 0;
    var networkIP = `192.168.${add3rd}.${add4th}`;
    array.forEach(function(element) {
        add4th = add4th + 1;
        var hostFirst = `192.168.${add3rd}.${add4th}`;
        add4th = add4th + element.amount;
        var hostLast = `192.168.${add3rd}.${add4th}`;
        add4th = add4th + 1;
        var broadcast = `192.168.${add3rd}.${add4th}`;
        calcNetworksInfo.push(
            {
                maskAddress: element.address,
                shortMask: element.shortAdd,
                networkIP: networkIP,
                firstHostIP: hostFirst,
                lastHostIP: hostLast,
                broadcastIP: broadcast,
                hosts: `${element.amount - 1} + R` // R -> router
            }
        );
        if(add4th === 256){
            add3rd = add3rd + 1;
            add4th = 0;
        }
        else{
            add4th = add4th + 1;
        }
        networkIP = `192.168.${add3rd}.${add4th}`;
    })

    req.networkInfo = calcNetworksInfo;
}