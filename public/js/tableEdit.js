$(document).ready(() => {
    const $tableID = $('#tableDiv');
   
    $('#table-add').click(() => {
        var rows = document.getElementById('table').getElementsByTagName('tr');

        const newTr = `
        <tr class="result-row">
            <th></td>
            <th>
                <input class="form-control" type="number" min="1" max="256" name="net" placeholder=0>
            </td>
            <th>
                <div class="row justify-content-end">
                    <button class="btn btn-danger tableButton disable-select" type="button" id='table-up'>
                        <i class="material-icons">keyboard_arrow_up</i>
                    <button class="btn btn-danger tableButton disable-select" type="button" id='table-down'>
                        <i class="material-icons">keyboard_arrow_down</i>
                    <button class="btn btn-danger tableButton disable-select" type="button" id='table-rm'>
                        <b> - </b>
                </div>
            </td>
        `;

        $('tbody').append(newTr);

        for (var i = 1, len = rows.length; i < len; i++){
            rows[i].children[0].textContent = i;
        }
    });
   
    $tableID.on('click', '#table-rm', function () {
        var rows = document.getElementById('table').getElementsByTagName('tr');

        $(this).parents('tr').detach();

        for (var i = 1, len = rows.length; i < len; i++){
            rows[i].children[0].textContent = i;
        }
    });

    $tableID.on('click', '#table-up', function () {
        var rows = document.getElementById('table').getElementsByTagName('tr');

        const $row = $(this).parents('tr');
     
        if ($row.index() === 0) {
            return;
        }
     
        $row.prev().before($row.get(0));

        for (var i = 1, len = rows.length; i < len; i++){
            rows[i].children[0].textContent = i;
        }
    });
     
    $tableID.on('click', '#table-down', function () {
        var rows = document.getElementById('table').getElementsByTagName('tr');

        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));

        
        for (var i = 1, len = rows.length; i < len; i++){
            rows[i].children[0].textContent = i;
        }
    });
});