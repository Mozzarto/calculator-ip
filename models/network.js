var networks = [ 
    { address: "255.255.255.0", shortAdd: "/24", amount: 254 }, 
    { address: "255.255.255.128", shortAdd: "/25", amount: 126 }, 
    { address: "255.255.255.192", shortAdd: "/26", amount: 62 }, 
    { address: "255.255.255.224", shortAdd: "/27", amount: 30 },
    { address: "255.255.255.240", shortAdd: "/28", amount: 14 },
    { address: "255.255.255.248", shortAdd: "/29", amount: 6 },
    { address: "255.255.255.252", shortAdd: "/30", amount: 2 },
];

module.exports = {
    getBiggerNetwork: function(amount, callback) {
        var found = networks[0];
        networks.forEach(function(element) {
            if (element.amount >= amount) {
                found = element;
            }
            else {
                return found;
            }
        });

        return found;       
    },
  
    getSmallerNetwork: function(amount) {
        var found = networks.find(function(element) { 
            return element.amount <= amount; 
          }); 
        return found;   
    }
};