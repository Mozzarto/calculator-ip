const app = require('./app');

app.set('port', process.env.PORT || 8080);

const server = app.listen(app.get('port'), "127.0.0.1", () => {
    console.log(`Listening on ${server.address().address}:${server.address().port}`);
});