const express = require('express');
const router = express.Router();

const PagesController = require('../controllers/PagesController');
const CalculatorController = require('../controllers/CalculatorController');

router.get('/', PagesController.home);

router.get('/calculator', 
    CalculatorController.destroySession,
    CalculatorController.home);

router.get('/auto', 
    CalculatorController.auto);

router.get('/manual', 
    CalculatorController.manual);

router.get('/result', 
    CalculatorController.calculate,
    CalculatorController.result);

module.exports = router;